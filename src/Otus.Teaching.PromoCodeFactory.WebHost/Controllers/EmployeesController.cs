﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Exceptions;
using Otus.Teaching.PromoCodeFactory.WebHost.Configuration.Automapper;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IMapper _mapper;

        public EmployeesController(IRepository<Employee> employeeRepository, IMapper mapper)
        {
            _employeeRepository = employeeRepository ?? throw new ArgumentNullException(nameof(employeeRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            return employees.Select(x => _mapper.Map<EmployeeShortResponse>(x));
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            return _mapper.Map<EmployeeResponse>(employee);
        }

        /// <summary>
        /// Создать нового сотрудника
        /// </summary>
        /// <param name="profile">Профиль нового сотрудника</param>
        /// <returns>Созданный объект или код конфликта вставки</returns>
        [HttpPost]
        public async Task<ActionResult<EmployeeResponse>> CreateEmployee([FromBody] EmployeeRequest profile)
        {
            try
            {
                var employee = await _employeeRepository.CreateAsync(_mapper.Map<Employee>(profile));
                return _mapper.Map<EmployeeResponse>(employee);
            }
            catch (DataInsertionConflictException conflictException)
            {
                return Conflict("Conflicting identifiers: " + string.Join(", ", conflictException.ConflictingIdentifiers));
            }
        }

        /// <summary>
        /// Обновить профиль сотрудника
        /// </summary>
        /// <param name="profile">Профиль сотрудника</param>
        /// <returns>Обновленные данные</returns>
        [HttpPut]
        public async Task<ActionResult<EmployeeResponse>> UpdateEmployee([FromBody] EmployeeRequest profile)
        {
            var employee = await _employeeRepository.UpdateAsync(_mapper.Map<Employee>(profile));

            if (employee == null)
                return NotFound();
            
            return _mapper.Map<EmployeeResponse>(employee);
        }
        
        /// <summary>
        /// Удалить профиль сотрудника
        /// </summary>
        /// <returns>Удаленные данные</returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> DeleteEmployee(Guid id)
        {
            var employee = await _employeeRepository.DeleteByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            return _mapper.Map<EmployeeResponse>(employee);
        }
        
        /// <summary>
        /// Удалить всех сотрудников
        /// </summary>
        /// <returns>Удаленные данные</returns>
        [HttpDelete]
        public async Task<IEnumerable<EmployeeResponse>> DeleteAllEmployees()
        {
            var employees = await _employeeRepository.DeleteAllAsync();

            return employees.Select(e => _mapper.Map<EmployeeResponse>(e));
        }
    }
}