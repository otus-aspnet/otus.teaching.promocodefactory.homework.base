﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Exceptions;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        private readonly IMemoryCache _dataStore;

        private static string GetCacheKey => $"in_memory_repo#{typeof(T).FullName}";
        
        private static readonly object InitializationLock = new object();

        public InMemoryRepository(IMemoryCache dataStore)
        {
            _dataStore = dataStore ?? throw new ArgumentNullException(nameof(dataStore));
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            IEnumerable<KeyValuePair<Guid, T>> result = null;
            if (_dataStore.TryGetValue(GetCacheKey, out var value))
                result = value as IEnumerable<KeyValuePair<Guid, T>>;

            return Task.FromResult((result ?? new List<KeyValuePair<Guid, T>>()).Select(v => v.Value));
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            if (!_dataStore.TryGetValue(GetCacheKey, out var value))
                return Task.FromResult(default(T));
            
            if(!(value is IReadOnlyDictionary<Guid, T> data))
                return Task.FromResult(default(T));
            
            return Task.FromResult(data.TryGetValue(id, out var result) ? result : default(T));
        }

        public Task<T> CreateAsync(T record)
        {
            if(record == null)
                throw new ArgumentNullException(nameof(record));

            var store = GetOrInitializeStore();

            if(TryInsertNewEntity(store, record))
                return Task.FromResult(record);
            
            throw new DataInsertionConflictException(record.Id.ToString());
        }

        public Task<IEnumerable<T>> CreateAllAsync(IEnumerable<T> records)
        {
            if(records == null)
                throw new ArgumentNullException(nameof(records));

            if (!records.Any())
                return Task.FromResult(records);
            
            var store = GetOrInitializeStore();
            var conflicts = new List<string>();

            foreach (var record in records)
            {
                if(!TryInsertNewEntity(store, record))
                    conflicts.Add(record.Id.ToString());
            }

            if (conflicts.Count > 0)
                throw new DataInsertionConflictException(conflicts.ToArray());

            return Task.FromResult(records);
        }

        public Task<T> UpdateAsync(T record)
        {
            if(record == null)
                throw new ArgumentNullException(nameof(record));
            
            var store = GetOrInitializeStore();
            if(!store.TryGetValue(record.Id, out var existingRecord))
                return Task.FromResult(default(T));

            return Task.FromResult(
                store.TryUpdate(record.Id, record, existingRecord) 
                    ? record 
                    : default(T));
        }

        public Task<IEnumerable<T>> DeleteAllAsync()
        {
            var store = GetOrInitializeStore();
            var result = store.Select(r => r.Value).ToList();
            
            store.Clear();
            
            return Task.FromResult<IEnumerable<T>>(result);
        }

        public Task<T> DeleteByIdAsync(Guid id)
        {
            var store = GetOrInitializeStore();
            return Task.FromResult(store.TryRemove(id, out var deleted) ? deleted : default(T));
        }

        private ConcurrentDictionary<Guid, T> GetOrInitializeStore()
        {
            //Double check locking
            if (!_dataStore.TryGetValue(GetCacheKey, out var value))
            {
                lock (InitializationLock)
                {
                    if (!_dataStore.TryGetValue(GetCacheKey, out value))
                    {
                        value = new ConcurrentDictionary<Guid, T>();
                        _dataStore.Set(GetCacheKey, value, new MemoryCacheEntryOptions().SetPriority(CacheItemPriority.NeverRemove));
                    }
                }
            }

            return value as ConcurrentDictionary<Guid, T>
                   ?? throw new InvalidOperationException(
                       $"Inconsistent application state: unsupported internal storage type: {value.GetType().FullName}");
        }

        private static T EnrichEntity(T entity)
        {
            if(Guid.Empty == entity.Id)
                entity.Id = Guid.NewGuid();

            return entity;
        }

        private static bool TryInsertNewEntity(ConcurrentDictionary<Guid, T> store, T record)
        {
            EnrichEntity(record);
            return store.TryAdd(record.Id, record);
        }
    }
}