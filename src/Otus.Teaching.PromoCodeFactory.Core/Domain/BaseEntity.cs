﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}