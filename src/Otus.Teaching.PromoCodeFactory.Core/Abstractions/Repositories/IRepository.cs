﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);

        Task<IEnumerable<T>> CreateAllAsync(IEnumerable<T> records);
        
        Task<T> CreateAsync(T record);

        Task<T> UpdateAsync(T record);

        Task<IEnumerable<T>> DeleteAllAsync();
        
        Task<T> DeleteByIdAsync(Guid id);
    }
}